<?php 
require_once('Medoo.php');
	class Post_Meta{
		public $db;
		private $table = "post_meta";

		public function __construct( $db_configs ){
			$this->db = new Medoo( $db_configs );
		}



public function create($post_id, $meta_key, $meta_value,$remote_id = null){        
       $query = $this->db->insert($this->table, [
			"post_id" => $post_id,
			"meta_key" => $meta_key,
			"meta_value" => $meta_value
                ]);

       return $this->db->id()> 0 ? true:false;

}

public function read( $columns = [] ){
	if( $columns != [] ){
	$read = $this->db->select($this->table, [$columns], []);
	}else{
	$read = $this->db->select($this->table, "*" );
	}
	return  !empty($read) ? json_encode($read) : [];	
}


public function update( $update_id,$post_id, $meta_key, $meta_value ){
$id = ( int ) $update_id;
$update = $this->db->update($this->table, [
			"post_id" => $post_id,
			"meta_key" => $meta_key,
			"meta_value" => $meta_value
], [
	"id" => $id
]);
return $update;
}

    // Delete record
    public function delete($id){
        $delete_id = (int) $id;

        $delete = $this->db->delete($this->table, [ 
            "id" => $delete_id 
        ]);
        return $delete > 0 ? true:false;
    }



}

	$app_user = new Post_Meta([
		"database_type" => "mysql",
		"database_name" => "mydb",
		"server" => "localhost",
		"username" => "metajua",
		"password" => "metajua",
		"charset" => "utf8"

	]); 
?>